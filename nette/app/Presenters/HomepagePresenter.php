<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model;
use App\Forms;
use Nette\Application\UI\Form;

final class HomepagePresenter extends BasePresenter
{

	/** @var Forms\ShortenUrlFormFactory */
	private Forms\ShortenUrlFormFactory $shortenUrlFactory;

	/** @var Model\UrlShortener */
	private Model\UrlShortener $urlShortener;

	public function __construct(Forms\ShortenUrlFormFactory $shortenUrlFactory, Model\UrlShortener $urlShortener)
	{
		$this->shortenUrlFactory = $shortenUrlFactory;
		$this->urlShortener = $urlShortener;
	}

	protected function createComponentShortenUrlForm(): Form
	{
		$form = ($this->shortenUrlFactory)->create();
		
		$form->onSuccess[] = function(Form $form, \stdClass $values): void
		{
			try{
				$return = $this->urlShortener->save($values);
				
				$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/r/' . $return;
				$this->flashMessage('Your short url is: <a href="'.$url.'">'.$url.'</a>', 'success');
				$this->redirect('this');
			}catch(Model\SlugTooLong $e){
				$form->addError('Slug cannot be longer than 10 characters.');
				return;
			}catch(Model\SlugAlphaNumFormat $e)
			{
				$form->addError('Slug can only contain alpha-numeric characters.');
				return;
			}catch(Model\DuplicatedSlug $e)
			{
				$form->addError('Slug already in use.');
				return;
			}catch(Model\UrlFormatError $e)
			{
				$form->addError('URL Format error.');
				return;
			}
		};

		return $form;
	}

	public function actionRedirect($slug): void
	{
		$url = $this->urlShortener->load($slug);
		$this->redirectUrl($url);
	}

}