<?php

declare(strict_types=1);

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;

final class ShortenUrlFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private FormFactory $factory;

	public function __construct(FormFactory $factory)
	{
		$this->factory = $factory;
	}

	public function create(): Form
	{
		$form = $this->factory->create();
		
		$form->addText('url', 'URL:')
			->addRule(Form::URL, 'must be a valid URL')
			->addRule(Form::MAX_LENGTH, 'URl cannot be longer than 255 characters.', 255)
			->setRequired();

		$form->addText('slug', 'SLUG:');

		$form->addSubmit('create', 'Create');

		return $form;
	}
}