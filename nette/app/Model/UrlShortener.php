<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Utils\Random;
use Nette\Utils\DateTime;

final class UrlShortener
{
	use Nette\SmartObject;

	/** @var Nette\Database\Connection */
	private $database;

	public function __construct(Nette\Database\Connection $database)
	{
		$this->database = $database;	
	}

	public function save(object $values): string
	{

		// This is bullshit
		// for example http://example.com/"><script>alert(document.cookie)</script> is accepted
		// I will check this later and possibly use regex anyways
		if(!filter_var($values->url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED))
		{
			throw new UrlFormatError;
		}

		// Slug cannot be longer then 10 characters
		if(strlen($values->slug) > 10)
		{
			throw new SlugTooLong;
		}

		// If slug is empty generate random slug
		if(empty($values->slug))
		{
			$slug = Random::generate(7);
		}else{
			$slug = $values->slug;
		}

		// Slug can only contain alpha-numeric characters
		if(!ctype_alnum($slug))
		{
			throw new SlugAlphaNumFormat;
		}

		// Check for duplicated slug
		$sql = $this->database->fetch('SELECT * FROM urls WHERE slug = ?', $slug);
		if( $sql )
		{
			throw new DuplicatedSlug;
		}

		$url = $values->url;

		$sql = $this->database->query('INSERT INTO urls ?', [
			'real_url' => $url,
			'slug' => $slug,
			'created_by_ip' => $_SERVER['REMOTE_ADDR'],
			'created_by_ua' => $_SERVER['HTTP_USER_AGENT'],
			'created_at' => new \Datetime
		]);

		return $slug;
	}

	public function load(string $slug): string
	{
		$real_url = $this->database->fetchField('SELECT real_url FROM urls WHERE slug = ?', $slug);
		if($real_url)
		{
			return $real_url;
		}else
		{
			return '__not_found';
		}
	}
}

class SlugTooLong extends \Exception
{
}

class SlugAlphaNumFormat extends \Exception
{
}

class DuplicatedSlug extends \Exception
{
}

class UrlFormatError extends \Exception
{
}